package com.hcl.shopforhome.contoller;

import java.util.List;




import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.message.ResponseMessage;
import com.hcl.shopforhome.model.Category;
import com.hcl.shopforhome.service.ICategoryService;



@RestController
public class CategoryController {

	@Autowired
	ICategoryService categoryService;

	// Add Category
	@PostMapping(value = "/category")
	public ResponseEntity<Category> addCategory(@RequestBody Category category) {

		return new ResponseEntity<Category>(categoryService.addCategory(category), HttpStatus.CREATED);
	}

	// Get All Category
	@GetMapping(value = "/category")
	public ResponseEntity<List<Category>> getAllCategory() {

		return new ResponseEntity<List<Category>>(categoryService.getAllCategory(), HttpStatus.OK);
	}

	// Get Category By ID
	@GetMapping(value = "/category/{id}")
	public ResponseEntity<Category> getCategoryById(@PathVariable("id") int id) throws ProjectException {

		return new ResponseEntity<Category>(categoryService.getCategoryById(id), HttpStatus.OK);
	}

	// Update Category
	@PutMapping(value = "/category/{id}")
	public ResponseEntity<Category> updateCategory(@RequestBody Category category,@PathVariable int id) throws ProjectException {

		return new ResponseEntity<Category>(categoryService.updateCategory(category,id), HttpStatus.OK);
	}

	// Delete Category By Id
	@DeleteMapping(value = "/category/{id}")
	public ResponseEntity<ResponseMessage> deleteCategoryById(@PathVariable("id") int id) throws ProjectException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(categoryService.deleteCategoryById(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	
		
	@ExceptionHandler(ProjectException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
	}

}

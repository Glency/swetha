package com.hcl.shopforhome.contoller;

import java.util.List;




import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.message.ResponseMessage;
import com.hcl.shopforhome.model.Order;
import com.hcl.shopforhome.service.IOrderService;



@RestController
public class OrderController {

	@Autowired
	IOrderService orderService;

	// Add Order
	@PostMapping(value = "/order")
	public ResponseEntity<Order> addOrder(@RequestBody Order order) {

		return new ResponseEntity<Order>(orderService.addOrder(order), HttpStatus.CREATED);
	}

	// Get All Order
	@GetMapping(value = "/order")
	public ResponseEntity<List<Order>> getAllOrder() {

		return new ResponseEntity<List<Order>>(orderService.getAllOrder(), HttpStatus.OK);
	}

	// Get Order By ID
	@GetMapping(value = "/order/{id}")
	public ResponseEntity<Order> getOrderById(@PathVariable("id") int id) throws ProjectException {

		return new ResponseEntity<Order>(orderService.getOrderById(id), HttpStatus.OK);
	}

	// Update Order
	@PutMapping(value = "/order/{id}")
	public ResponseEntity<Order> updateOrder(@RequestBody Order order,@PathVariable int id) throws ProjectException {

		return new ResponseEntity<Order>(orderService.updateOrder(order,id), HttpStatus.OK);
	}

	// Delete Order By Id
	@DeleteMapping(value = "/order/{id}")
	public ResponseEntity<ResponseMessage> deleteOrderById(@PathVariable("id") int id) throws ProjectException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(orderService.deleteOrderById(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	
		
	@ExceptionHandler(ProjectException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
	}

}

package com.hcl.shopforhome.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Cart;
import com.hcl.shopforhome.model.OrderItems;
import com.hcl.shopforhome.model.Products;
import com.hcl.shopforhome.service.ICartService;

@RestController
public class CartController {

	@Autowired
	ICartService cartService;
	
	@PostMapping(value = "/cart")
	public Cart addToCart(@RequestBody Cart cart,@RequestBody OrderItems products) {
		return cartService.addToCart(products, cart);
	}
	
	@DeleteMapping(value="/cart/{id}")
	public String deleteFromCart(@PathVariable(value="id") int cartId,@RequestBody OrderItems products) throws ProjectException {
		return cartService.deleteFromCart(products, cartId);
	}
	
	@GetMapping(value="cart/{userId}")
	public Cart myCart(@PathVariable(value="userId") int userId) throws ProjectException {
		return cartService.getCartByUserId(userId);
	}

}

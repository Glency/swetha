package com.hcl.shopforhome.contoller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.file.CSVHelper;
import com.hcl.shopforhome.file.FileUploadUtil;
import com.hcl.shopforhome.message.ResponseMessage;
import com.hcl.shopforhome.message.ResponseMessageCSV;
import com.hcl.shopforhome.model.Products;
import com.hcl.shopforhome.service.IProductsService;


@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
public class ProductsController {

	@Autowired
	IProductsService productsService;
	


	// Add Products
	@PostMapping(value = "/products")
	public ResponseEntity<Products> addProduct(@RequestParam Products product,@RequestParam("image") MultipartFile multipartFile) throws IOException {

		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		product.setPhotos(fileName);
         
		Products savedProducts = productsService.addProduct(product);
 
        String uploadDir = "product-photos/" + savedProducts.getId();
 
        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
        
		return new ResponseEntity<Products>(productsService.addProduct(product), HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/product-photos/{productId}/{imageName}")
	public ResponseEntity<byte[]> getImage(@PathVariable("productId") int Id,@PathVariable("imageName") String Name) throws ProjectException {
		   byte[] image = new byte[0];
	        try {
	            image = FileUtils.readFileToByteArray(new File("product-photos/"+Id+"/"+Name));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(image);
	    }
	

	

	// Get All Products
	@GetMapping(value = "/products")
	public ResponseEntity<List<Products>> getAllProducts() {
		  try {
		      List<Products> productList = productsService.getAllProducts();

		      if (productList.isEmpty()) {
		        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		      }

		      return new ResponseEntity<>(productList, HttpStatus.OK);
		    } catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		  
	}

	// Get Products By ID
	@GetMapping(value = "/products/{id}")
	public ResponseEntity<Products> getProductById(@PathVariable("id") int id) throws ProjectException {

		return new ResponseEntity<Products>(productsService.getProductById(id), HttpStatus.OK);
	}

	// Update Products
	@PutMapping(value = "/products/{id}")
	public ResponseEntity<Products> updateProduct(@RequestBody Products product,@PathVariable int id) throws ProjectException {

		return new ResponseEntity<Products>(productsService.updateProduct(product,id), HttpStatus.OK);
	}

	// Delete Products By Id
	@DeleteMapping(value = "/products/{id}")
	public ResponseEntity<ResponseMessage> deleteProductById(@PathVariable("id") int id) throws ProjectException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(productsService.deleteProductById(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	// Add Products To Cart
		@PostMapping(value = "/addProductsToWishlist/{userId}/{productId}")
		public ResponseEntity<String> addProductsToWishlist(@PathVariable int userId, @PathVariable int productId)
				throws ProjectException {
			return new ResponseEntity<String>(productsService.addProductsToWishlist(userId, productId), HttpStatus.OK);
		}

		// Delete Products From Cart
		@DeleteMapping(value = "/deleteProductsFromWishlist/{userId}/{productId}")
		public ResponseEntity<String> deleteProductsFromWishlist(@PathVariable int userId, @PathVariable int productId)
				throws ProjectException {
			return new ResponseEntity<>(productsService.deleteProductsFromWishlist(userId, productId), HttpStatus.OK);
		}
		
		@GetMapping(value="/products/sortByName")
		public List<Products> sortByName(){
			return productsService.getSortedProductsByProductName();
		}
		
		@PostMapping("/uploadCSV")
		  public ResponseEntity<ResponseMessageCSV> uploadFile(@RequestParam("file") MultipartFile file) {
		    String message = "";

		    if (CSVHelper.hasCSVFormat(file)) {
		      try {
		    	  productsService.saveProductFromCSV(file);

		        message = "Uploaded the file successfully: " + file.getOriginalFilename();
		        return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessageCSV(message));
		      } catch (Exception e) {
		        message = "Could not upload the file: " + file.getOriginalFilename() + "!";
		        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessageCSV(message));
		      }
		    }

		    message = "Please upload a csv file!";
		    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessageCSV(message));
		  }
		  

		
	@ExceptionHandler(ProjectException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
	}

}

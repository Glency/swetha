package com.hcl.shopforhome.contoller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.message.ResponseMessage;
import com.hcl.shopforhome.model.User;
import com.hcl.shopforhome.service.IUserService;

@RestController
public class UserController {

	@Autowired
	IUserService userService;

	
	@PostMapping(value = "/register")
	public ResponseEntity<User> addUser(@RequestBody User user) {

		return new ResponseEntity<User>(userService.addUser(user), HttpStatus.CREATED);
	}

	
	@GetMapping(value = "/users")
	public ResponseEntity<List<User>> getAllUsers() {

		return new ResponseEntity<List<User>>(userService.getAllUsers(), HttpStatus.OK);
	}

	
	@GetMapping(value = "/users/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") int id) throws ProjectException {

		return new ResponseEntity<User>(userService.getUserById(id), HttpStatus.OK);
	}

	
	@PutMapping(value = "/users/{id}")
	public ResponseEntity<User> updateUser(@RequestBody User user,@PathVariable int id) throws ProjectException {

		return new ResponseEntity<User>(userService.updateUser(user,id), HttpStatus.OK);
	}


	@DeleteMapping(value = "/users/{id}")
	public ResponseEntity<ResponseMessage> deleteUserById(@PathVariable("id") int id) throws ProjectException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(userService.deleteUserById(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	@ExceptionHandler(ProjectException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
	}

}

package com.hcl.shopforhome.contoller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.message.ResponseMessage;
import com.hcl.shopforhome.model.Stock;
import com.hcl.shopforhome.service.IStockService;
@RestController
public class StockController {
	

		@Autowired
		IStockService stockService;

		// Add Category
		@PostMapping(value = "/stock")
		public ResponseEntity<Stock> addStock(@RequestBody Stock stock) {

			return new ResponseEntity<Stock>(stockService.addStock(stock), HttpStatus.CREATED);
		}

		// Get All Category
		@GetMapping(value = "/stock")
		public ResponseEntity<List<Stock>> getAllStock() {

			return new ResponseEntity<List<Stock>>(stockService.getAllStock(), HttpStatus.OK);
		}

		// Get Category By ID
		@GetMapping(value = "/stock/{id}")
		public ResponseEntity<Stock> getStockById(@PathVariable("id") int id) throws ProjectException {

			return new ResponseEntity<Stock>(stockService.getStockById(id), HttpStatus.OK);
		}

		// Update Category
		@PutMapping(value = "/stock/{id}")
		public ResponseEntity<Stock> updateCategory(@RequestBody Stock stock,@PathVariable int id) throws ProjectException {

			return new ResponseEntity<Stock>(stockService.updateStock(stock,id), HttpStatus.OK);
		}

		// Delete Category By Id
		@DeleteMapping(value = "/stock/{id}")
		public ResponseEntity<ResponseMessage> deleteStockById(@PathVariable("id") int id) throws ProjectException {
			ResponseMessage rm = new ResponseMessage();
			rm.setMessage(stockService.deleteStockById(id));
			return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
		}

		
			
		@ExceptionHandler(ProjectException.class)
		public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
			ResponseMessage rm = new ResponseMessage();
			rm.setMessage(ex.getMessage());
			rm.setErrorCode(404);
			return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
		}

	}


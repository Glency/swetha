package com.hcl.shopforhome.contoller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.message.ResponseMessage;
import com.hcl.shopforhome.model.OrderItems;
import com.hcl.shopforhome.service.IOrderItemsService;

@RestController
public class OrderItemsController {
	@Autowired
	IOrderItemsService orderItemsService;

	// Add Order
	@PostMapping(value = "/orderItems")
	public ResponseEntity<OrderItems> addOrderItems(@RequestBody OrderItems orderItems) throws ProjectException {

		return new ResponseEntity<OrderItems>(orderItemsService.addOrderItems(orderItems), HttpStatus.CREATED);
	}

	// Get All Order
	@GetMapping(value = "/orderItems")
	public ResponseEntity<List<OrderItems>> getAllOrderItems() {

		return new ResponseEntity<List<OrderItems>>(orderItemsService.getAllOrderItems(), HttpStatus.OK);
	}

	// Get Order By ID
	@GetMapping(value = "/orderItems/{id}")
	public ResponseEntity<OrderItems> getOrderItemsById(@PathVariable("id") int id) throws ProjectException {

		return new ResponseEntity<OrderItems>(orderItemsService.getOrderItemsById(id), HttpStatus.OK);
	}

	// Update Order
	@PutMapping(value = "/orderItems/{id}")
	public ResponseEntity<OrderItems> updateOrderItems(@RequestBody OrderItems orderItems,@PathVariable int id) throws ProjectException {

		return new ResponseEntity<OrderItems>(orderItemsService.updateOrderItems(orderItems,id), HttpStatus.OK);
	}

	// Delete Order By Id
	@DeleteMapping(value = "/orderItems/{id}")
	public ResponseEntity<ResponseMessage> deleteOrderItemsById(@PathVariable("id") int id) throws ProjectException {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(orderItemsService.deleteOrderItemsById(id));
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.OK);
	}

	
		
	@ExceptionHandler(ProjectException.class)
	public ResponseEntity<ResponseMessage> handleEmployeeIdException(HttpServletRequest request, Exception ex) {
		ResponseMessage rm = new ResponseMessage();
		rm.setMessage(ex.getMessage());
		rm.setErrorCode(404);
		return new ResponseEntity<ResponseMessage>(rm, HttpStatus.NOT_FOUND);
	}

}

package com.hcl.shopforhome.file;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.springframework.web.multipart.MultipartFile;

import com.hcl.shopforhome.model.Products;


public class CSVHelper {
  public static String TYPE = "text/csv";
  static String[] HEADERs = { "Id", "Productname", "ProductPrice","Brand","Photo"};

  public static boolean hasCSVFormat(MultipartFile file) {
    if (TYPE.equals(file.getContentType())
    		|| file.getContentType().equals("application/vnd.ms-excel")) {
      return true;
    }

    return false;
  }

  public static List<Products> csvToProductList(InputStream is) {
    try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        CSVParser csvParser = new CSVParser(fileReader,
            CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

      List<Products> ProductsList = new ArrayList<>();

      Iterable<CSVRecord> csvRecords = csvParser.getRecords();
      
      for (CSVRecord csvRecord : csvRecords) {
    	  
          Products product = new Products();

    	 product.setId((int)Long.parseLong(csvRecord.get("Id")));
    	 product.setProductname(csvRecord.get("Productname"));
    	 product.setProductprice(Double.parseDouble(csvRecord.get("ProductPrice")));
    	 product.setBrand(csvRecord.get("Brand"));
    	 product.setPhotos(csvRecord.get("Photo"));
    	 ProductsList.add(product);
      }
              
      return ProductsList;
    } catch (IOException e) {
      throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
    }
  }

  public static ByteArrayInputStream productListToCSV(List<Products> productList) {
    final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);

    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
        CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format);) {
      for (Products product : productList) {
        List<Object> data = Arrays.asList(
              String.valueOf(product.getId()),
              product.getProductname(),
             product.getProductprice()
            );

        csvPrinter.printRecord(data);
      }

      csvPrinter.flush();
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new RuntimeException("fail to import data to CSV file: " + e.getMessage());
    }
  }
}
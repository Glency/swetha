package com.hcl.shopforhome.message;

public class ResponseMessageCSV {
	
	  private String message;

	  public ResponseMessageCSV(String message) {
	    this.message = message;
	  }

	  public String getMessage() {
	    return message;
	  }

	  public void setMessage(String message) {
	    this.message = message;
	  }
	}
	


package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Products;


@Service
public interface IProductsService {

	public Products addProduct(Products product);

	public List<Products> getAllProducts();

	public Products getProductById(int productId) throws ProjectException;

	public Products updateProduct(Products product,int id) throws ProjectException;

	public String deleteProductById(int productId) throws ProjectException;

	public String addProductsToWishlist(int userId, int productId) throws ProjectException;

	public List<Products> getProductsByUserId(int userId) throws ProjectException;

	String deleteProductsFromWishlist(int userId, int productId) throws ProjectException;
	public List<Products> getSortedProductsByProductName();

	  public void saveProductFromCSV(MultipartFile file);
	  
	  public List<Products> searchByKeyword(String keyword);

}

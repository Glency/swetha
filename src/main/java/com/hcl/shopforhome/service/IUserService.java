package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.shopforhome.model.User;
import com.hcl.shopforhome.exception.ProjectException;
@Service
public interface IUserService {

	public User addUser(User user);

	public List<User> getAllUsers();

	public User getUserById(int userId) throws ProjectException;

	public User updateUser(User user,int id) throws ProjectException ;

	public String deleteUserById(int userId) throws ProjectException;

}

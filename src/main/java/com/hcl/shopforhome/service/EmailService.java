package com.hcl.shopforhome.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
	
	@Autowired
	JavaMailSender javaMailSender;

	public String sendEmail() {
		SimpleMailMessage message = new SimpleMailMessage();
		
		message.setFrom("varunnannan@gmail.com");
		message.setTo("sentamil08111999@gmail.com");
		message.setSubject("stack less than 10");
		message.setText("hi admin its found,"
				+ " that stack for the product in your application is less than 10"
				+ "please cheeck over it");
		
		javaMailSender.send(message);
		
		return "Mail sent successfully";
	}
	
}

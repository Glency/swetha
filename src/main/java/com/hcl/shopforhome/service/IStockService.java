package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Stock;

@Service
public interface IStockService {
	
	public Stock addStock(Stock stock);
	public List<Stock> getAllStock();
	public Stock getStockById(int stockId) throws ProjectException;
	public Stock updateStock(Stock stock,int id) throws ProjectException;
	public String deleteStockById(int stockId) throws ProjectException;
}

package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shopforhome.dao.IOrderRepository;
import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Order;



@Service
public class OrderServiceImpl implements IOrderService {

	@Autowired
	IOrderRepository orderRepository;


	@Override
	public Order addOrder(Order order) {
		// TODO Auto-generated method stub
		return orderRepository.save(order);
	}

	@Override
	public List<Order> getAllOrder() {
		// TODO Auto-generated method stub
		return orderRepository.findAll();
	}

	@Override
	public Order getOrderById(int orderId) throws ProjectException {
		// TODO Auto-generated method stub
		return orderRepository.findById(orderId).orElseThrow(() -> new ProjectException("Order Id not found"));
	}

	@Override
	public Order updateOrder(Order order,int id) throws ProjectException {
		// TODO Auto-generated method stub
		Order oldOrder = orderRepository.findById(id).get();

	    if (oldOrder!=null) {
	    	Order newOrder=new Order();
	    	newOrder.setOrderId(oldOrder.getOrderId());
	    	newOrder.setDate(order.getDate());
	    	newOrder.setItems(order.getItems());
	    	newOrder.setPrice(order.getPrice());
	    	newOrder.setUserId(order.getUserId());
	    	
		return orderRepository.saveAndFlush(newOrder);
	    }
	    else {
	    	throw new ProjectException("User Id not found");
	    }	
	}

	@Override
	public String deleteOrderById(int orderId) throws ProjectException {
		// TODO Auto-generated method stub
		Order order = orderRepository.findById(orderId).orElseThrow(() -> new ProjectException("Order Id not found"));
		orderRepository.delete(order);
		return "Deleted Successfully";
	}

	@Override
	public List<Order> getOrderByUserId(int userId) throws ProjectException {
		// TODO Auto-generated method stub
		return null;
	}

	
}



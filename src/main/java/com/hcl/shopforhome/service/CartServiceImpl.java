package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shopforhome.dao.ICartRepository;
import com.hcl.shopforhome.dao.IOrderItemsRepository;
import com.hcl.shopforhome.dao.IProductsRepository;
import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Cart;
import com.hcl.shopforhome.model.OrderItems;
import com.hcl.shopforhome.model.Products;

@Service
public class CartServiceImpl implements ICartService{

	@Autowired
	IOrderItemsRepository orderItemsRepository;
	
	@Autowired
	ICartRepository cartRepository;
	
	@Override
	public Cart addToCart(OrderItems product,Cart cart) {
		// TODO Auto-generated method stub
		double sum=0;
		OrderItems products = orderItemsRepository.findById(product.getOrderItemId()).orElse(null);
		List<OrderItems> productList=cart.getProduct();
		productList.add(products);
		cart.setProduct(productList);
		for(OrderItems productSum:productList) {
			sum+=((productSum.getProductname().getProductprice())*productSum.getQuantity());
		}
		cart.setSubTotal(sum);
		cart.setUserId(cart.getUserId());
	  
		return cartRepository.save(cart);
	}

	@Override
	public Cart getCartByUserId(int userId) throws ProjectException {
		// TODO Auto-generated method stub
		return cartRepository.getByUserId(userId);
	}


	@Override
	public String deleteFromCart(OrderItems product,int cartId) throws ProjectException {
		// TODO Auto-generated methocartRepositoryd stub
		double sum=0;
		Cart cart=cartRepository.getById(cartId);
		OrderItems products = orderItemsRepository.findById(product.getOrderItemId()).orElse(null);
		List<OrderItems> productList=cart.getProduct();
		productList.remove(products);
		cart.setProduct(productList);
		for(OrderItems productSum:productList) {
			sum+=((productSum.getProductname().getProductprice())*productSum.getQuantity());
		}
		cart.setSubTotal(sum);
		cart.setUserId(cart.getUserId());
	  
		 cartRepository.save(cart);
		return "deleted successfully";
	}

}

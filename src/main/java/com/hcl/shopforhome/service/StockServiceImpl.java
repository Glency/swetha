package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shopforhome.dao.IStockRepository;
import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Stock;

@Service
public class StockServiceImpl implements IStockService{

	@Autowired
	IStockRepository stockRepository;
	
	@Override
	public Stock addStock(Stock stock) {
		// TODO Auto-generated method stub
		return stockRepository.save(stock);
	}

	@Override
	public List<Stock> getAllStock() {
		// TODO Auto-generated method stub
		return stockRepository.findAll();
	}

	@Override
	public Stock getStockById(int stockId) throws ProjectException {
		// TODO Auto-generated method stub
		return stockRepository.findById(stockId).orElseThrow(() -> new ProjectException("Category Id not found"));
	}

	@Override
	public Stock updateStock(Stock stock,int id) throws ProjectException {
		// TODO Auto-generated method stub
		Stock oldStock = stockRepository.findById(id).get();

	    if (oldStock!=null) {
	    	Stock newStock=new Stock();
	    	newStock.setStockId(oldStock.getStockId());
	    	newStock.setProduct(stock.getProduct());
	    	newStock.setQuantity(stock.getQuantity());
 	
		return stockRepository.saveAndFlush(newStock);
	    }
	    else {
	    	throw new ProjectException("User Id not found");
	    }	
	}

	@Override
	public String deleteStockById(int stockId) throws ProjectException {
		// TODO Auto-generated method stub
		Stock stock = stockRepository.findById(stockId).orElseThrow(() -> new ProjectException("Stock Id not found"));
		stockRepository.delete(stock);
		return "Deleted Successfully";	}

}

package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Category;


@Service
public interface ICategoryService {

	public Category addCategory(Category category);

	public List<Category> getAllCategory();

	public Category getCategoryById(int categoryId) throws ProjectException;

	public Category updateCategory(Category category,int id) throws ProjectException;

	public String deleteCategoryById(int categoryId) throws ProjectException;



	
	
}

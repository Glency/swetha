package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shopforhome.dao.ICategoryRepository;
import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Category;



@Service
public class CategoryServiceImpl implements ICategoryService {

	@Autowired
	ICategoryRepository categoryRepository;


	@Override
	public Category addCategory(Category category) {
		// TODO Auto-generated method stub
		return categoryRepository.save(category);
	}

	@Override
	public List<Category> getAllCategory() {
		// TODO Auto-generated method stub
		return categoryRepository.findAll();
	}

	@Override
	public Category getCategoryById(int categoryId) throws ProjectException {
		// TODO Auto-generated method stub
		return categoryRepository.findById(categoryId).orElseThrow(() -> new ProjectException("Category Id not found"));
	}

	@Override
	public Category updateCategory(Category category,int id) throws ProjectException {
		// TODO Auto-generated method stub
		Category oldCategory = categoryRepository.findById(id).get();

	    if (oldCategory!=null) {
	    	Category newCategory=new Category();
	    	newCategory.setCategoryId(category.getCategoryId());
	    	newCategory.setCategoryName(category.getCategoryName());
	    	newCategory.setProduct(category.getProduct());
	    	
		return categoryRepository.saveAndFlush(newCategory);
	    }
	    else {
	    	throw new ProjectException("User Id not found");
	    }
		
	}

	@Override
	public String deleteCategoryById(int categoryId) throws ProjectException {
		// TODO Auto-generated method stub
		Category category = categoryRepository.findById(categoryId).orElseThrow(() -> new ProjectException("Category Id not found"));
		categoryRepository.delete(category);
		return "Deleted Successfully";
	}



	
}



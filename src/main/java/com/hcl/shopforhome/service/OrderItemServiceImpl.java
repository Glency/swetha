package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.shopforhome.dao.IOrderItemsRepository;
import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.OrderItems;
import com.hcl.shopforhome.model.Stock;

@Service
public class OrderItemServiceImpl implements IOrderItemsService{
	
	@Autowired
	IOrderItemsRepository orderItemsRepository;
	
	@Autowired
	IStockService stockservice;
	
	@Autowired
	EmailService emailService;
	
	@Override
	public OrderItems addOrderItems(OrderItems orderItems) throws ProjectException {
		// TODO Auto-generated method stub
		Stock stock= orderItems.getProductname().getStockId();
		int quantity= stock.getQuantity() - orderItems.getQuantity();
		if(stockservice.getStockById(stock.getStockId()) != null) {
			stock.setQuantity(quantity);
			stockservice.updateStock(stock,stock.getStockId());
			if(quantity<10) {
				emailService.sendEmail();
			}
		}
		return orderItemsRepository.save(orderItems);
	}

	@Override
	public List<OrderItems> getAllOrderItems() {
		// TODO Auto-generated method stub
		return orderItemsRepository.findAll();
	}

	@Override
	public OrderItems getOrderItemsById(int orderItemsId) throws ProjectException {
		// TODO Auto-generated method stub
		return orderItemsRepository.findById(orderItemsId).orElseThrow(() -> new ProjectException("Order Id not found"));
	}

	@Override
	public OrderItems updateOrderItems(OrderItems orderItems,int id) throws ProjectException {
		// TODO Auto-generated method stub
		OrderItems oldOrderItems = orderItemsRepository.findById(id).get();

	    if (oldOrderItems!=null) {
	    	OrderItems newOrderItems=new OrderItems();
	    	newOrderItems.setOrderId(orderItems.getOrderId());
	    	newOrderItems.setOrderItemId(oldOrderItems.getOrderItemId());
	    	newOrderItems.setProductname(orderItems.getProductname());
	    	newOrderItems.setQuantity(orderItems.getQuantity());
	    	
		return orderItemsRepository.saveAndFlush(newOrderItems);
	    }
	    else {
	    	throw new ProjectException("User Id not found");
	    }
		
	}

	@Override
	public String deleteOrderItemsById(int orderItemsId) throws ProjectException {
		// TODO Auto-generated method stub
		OrderItems orderItems = orderItemsRepository.findById(orderItemsId).orElseThrow(() -> new ProjectException("Order Id not found"));
		orderItemsRepository.delete(orderItems);
		return "Deleted Successfully";	}
	

	
}

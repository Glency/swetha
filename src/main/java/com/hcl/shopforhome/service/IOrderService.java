package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Order;


@Service
public interface IOrderService {

	public Order addOrder(Order order);

	public List<Order> getAllOrder();

	public Order getOrderById(int orderId) throws ProjectException;

	public Order updateOrder(Order order,int id) throws ProjectException;

	public String deleteOrderById(int orderId) throws ProjectException;

	public List<Order> getOrderByUserId(int userId) throws ProjectException;

	
	
}

package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hcl.shopforhome.dao.IUserRepository;
import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.User;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	IUserRepository userRepository;

	@Override
	public User addUser(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public User getUserById(int userId) throws ProjectException {
		// TODO Auto-generated method stub
		return userRepository.findById(userId).orElseThrow(() -> new ProjectException("User Id not found"));
	}

	@Override
	public User updateUser(User user,int id) throws ProjectException {
		// TODO Auto-generated method stub
		User oldUser = userRepository.findById(id).get();

	    if (oldUser!=null) {
	    	User newUser=new User();
	    	newUser.setId(oldUser.getId());
	    	newUser.setName(user.getName());
	    	newUser.setEmail(user.getEmail());
	    	newUser.setPassword(user.getPassword());
	    	newUser.setProducts(user.getProducts());
	    	newUser.setRoles(user.getRoles());
	    	newUser.setOrders(user.getOrders());
	    	newUser.setUsername(user.getUsername());
	 
		return userRepository.saveAndFlush(newUser);
	    }
	    else {
	    	throw new ProjectException("User Id not found");
	    }
	}

	@Override
	public String deleteUserById(int userId) throws ProjectException {
		// TODO Auto-generated method stub
		User user = userRepository.findById(userId).orElseThrow(() -> new ProjectException("User Id not found"));
		userRepository.delete(user);
		return "Deleted Successfully";
	}

}

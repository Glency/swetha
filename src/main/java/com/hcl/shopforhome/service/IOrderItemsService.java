package com.hcl.shopforhome.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.OrderItems;

@Service
public interface IOrderItemsService {
	
	public OrderItems addOrderItems(OrderItems orderItems) throws ProjectException;
	public List<OrderItems> getAllOrderItems();
	public OrderItems getOrderItemsById(int orderItemsId) throws ProjectException;
	public OrderItems updateOrderItems(OrderItems orderItems,int id) throws ProjectException;
	public String deleteOrderItemsById(int orderItemsId) throws ProjectException;

}

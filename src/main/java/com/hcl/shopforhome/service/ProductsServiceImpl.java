package com.hcl.shopforhome.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.hcl.shopforhome.dao.IProductsRepository;
import com.hcl.shopforhome.dao.IUserRepository;
import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.file.CSVHelper;
import com.hcl.shopforhome.model.Products;
import com.hcl.shopforhome.model.User;


@Service
public class ProductsServiceImpl implements IProductsService {

	@Autowired
	IProductsRepository productsRepository;
	@Autowired
	IUserRepository userRepository;

	@Override
	public Products addProduct(Products product) {
		// TODO Auto-generated method stub
		return productsRepository.save(product);
	}

	@Override
	public List<Products> getAllProducts() {
		// TODO Auto-generated method stub
		return productsRepository.findAll();
	}

	@Override
	public Products getProductById(int productId) throws ProjectException {
		// TODO Auto-generated method stub
		return productsRepository.findById(productId).orElseThrow(() -> new ProjectException("Product Id not found"));
	}

	@Override
	public Products updateProduct(Products product,int id) throws ProjectException{
		// TODO Auto-generated method stub
		Products oldProduct = productsRepository.findById(id).get();

	    if (oldProduct!=null) {
	    	Products newProduct=new Products();
	    	newProduct.setId(oldProduct.getId());
	    	newProduct.setProductname(product.getProductname());
	    	newProduct.setProductprice(product.getProductprice());
	    	newProduct.setUsers(product.getUsers());
	    	newProduct.setCategoryId(product.getCategoryId());
	 
		return productsRepository.saveAndFlush(newProduct);
	    }
	    else {
	    	throw new ProjectException("User Id not found");
	    }
	}

	@Override
	public String deleteProductById(int productId) throws ProjectException {
		// TODO Auto-generated method stub
		Products product = productsRepository.findById(productId).orElseThrow(() -> new ProjectException("Product Id not found"));
		productsRepository.delete(product);
		return "Deleted Successfully";
	}

	@Override
	public String addProductsToWishlist(int userId, int productId) throws ProjectException {
		// TODO Auto-generated method stub
		User user = userRepository.findById(userId).orElseThrow(() -> new ProjectException("User Id not found"));
		Products product = productsRepository.findById(productId).orElseThrow(() -> new ProjectException("Product Id not found"));
		List<Products> ProductsList = user.getProducts();
		ProductsList.add(product);
		user.setProducts(ProductsList);
		userRepository.save(user);
		return "Product Added To Wishlist";
	}

	public String deleteProductsFromWishlist(int userId, int productId) throws ProjectException {
		// TODO Auto-generated method stub
		User user = userRepository.findById(userId).orElseThrow(() -> new ProjectException("User Id not found"));
		Products product = productsRepository.findById(productId).orElseThrow(() -> new ProjectException("Product Id not found"));
		List<Products> ProductsList = user.getProducts();
		ProductsList.removeIf(n -> (n.getId() == product.getId()));
		user.setProducts(ProductsList);
		userRepository.save(user);
		return "Product Deleted From Wishlist";
	}

	@Override
	public List<Products> getProductsByUserId(int userId) throws ProjectException {
		// TODO Auto-generated method stub
		User user = userRepository.findById(userId).orElseThrow(() -> new ProjectException("User Id not found"));
		return user.getProducts();
	}

	@Override
	public List<Products> getSortedProductsByProductName() {
		// TODO Auto-generated method stub
		return productsRepository.findAllByOrderProductnameAsc();
	}

	  public void saveProductFromCSV(MultipartFile file) {
		    try {
		      List<Products> productList = CSVHelper.csvToProductList(file.getInputStream());
		      productsRepository.saveAll(productList);
		    } catch (IOException e) {
		      throw new RuntimeException("fail to store csv data: " + e.getMessage());
		    }
		  }

	@Override
	public List<Products> searchByKeyword(String keyword) {
		// TODO Auto-generated method stub
		return productsRepository.findByKeyword(keyword);
	}
		 
}

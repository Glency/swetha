package com.hcl.shopforhome.service;

import org.springframework.stereotype.Service;

import com.hcl.shopforhome.exception.ProjectException;
import com.hcl.shopforhome.model.Cart;
import com.hcl.shopforhome.model.OrderItems;

@Service
public interface ICartService {
	
	public Cart addToCart(OrderItems products,Cart cart);
	public Cart getCartByUserId(int userId) throws ProjectException;
	public String deleteFromCart(OrderItems product,int cartId) throws ProjectException;
}

package com.hcl.shopforhome.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
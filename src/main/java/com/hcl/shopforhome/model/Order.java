package com.hcl.shopforhome.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@AllArgsConstructor
@Table(name = "userorder")
public class Order {
	@Id
	@Column(name = "order_id")
	private int orderId;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User userId;

	@OneToMany(mappedBy = "orderId", cascade = CascadeType.ALL)
	private List<OrderItems> items;

	@Column(name = "price")
	private double price;

	@Column(name="order_date")
	private Date date;
}

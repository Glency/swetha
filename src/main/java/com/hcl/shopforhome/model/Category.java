package com.hcl.shopforhome.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@Entity
@AllArgsConstructor
public class Category {

	@Id
	@Column(name = "id")
	@GeneratedValue
	private int categoryId;

	@Column(name = "name")
	private String categoryName;

	@OneToMany(mappedBy = "categoryId")
	private List<Products> product;
}

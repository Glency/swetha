package com.hcl.shopforhome.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@AllArgsConstructor
public class Stock {
	@Id
	@Column(name = "id")
	@GeneratedValue
	private int stockId;

	@OneToOne(mappedBy = "stockId")
	private Products product;
	
	private int quantity;
	
}

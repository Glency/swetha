package com.hcl.shopforhome.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="product")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Products
 {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String productname;
	private double productprice;
	private String brand;
	@Column(nullable = true, length = 64)
    private String photos;
	
	@ManyToOne
	@JoinColumn(name = "category_id")
	private Category categoryId;
	
	@JsonIgnore
	@ManyToMany(targetEntity = User.class, mappedBy = "Products", cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH})
	private List<User> users;
	
	@OneToOne
	@JoinColumn(name = "stockId")
	private Stock stockId;
	
	@OneToOne
	@JoinColumn(name = "orderItemId")
	private OrderItems orderItemId;
	
	@Transient
    public String getPhotosImagePath() {
        if (photos == null || id == 0) return null;
         
        return "/product-photos/" + id + "/" + photos;
    }
	
 }
package com.hcl.shopforhome.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.shopforhome.model.User;

@Repository
public interface IUserRepository extends JpaRepository<User, Integer>{

}
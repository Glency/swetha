package com.hcl.shopforhome.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.shopforhome.model.Stock;

@Repository
public interface IStockRepository extends JpaRepository<Stock, Integer> {

}

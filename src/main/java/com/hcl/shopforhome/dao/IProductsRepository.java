package com.hcl.shopforhome.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hcl.shopforhome.model.Products;



@Repository
public interface IProductsRepository extends JpaRepository<Products, Integer> {

	@Query("Select p FROM Products p ORDER BY p.productname ASC")
	List<Products> findAllByOrderProductnameAsc();
	
	
//	@Query(value = "select * from Product p where p.productname like %:keyword% or p.brand like %:keyword%", nativeQuery = true)

//	List<Products> findByKeyword(@Param("keyword") String keyword);
	
	@Query("SELECT p FROM Products p WHERE CONCAT(p.productname, p.brand,  p.productprice) LIKE %?1%")
	List<Products> findByKeyword(String keyword);

}

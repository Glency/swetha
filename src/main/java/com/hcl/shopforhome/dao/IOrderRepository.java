package com.hcl.shopforhome.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.hcl.shopforhome.model.Order;



@Repository
public interface IOrderRepository extends JpaRepository<Order, Integer> {

}

package com.hcl.shopforhome.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.shopforhome.model.Cart;

public interface ICartRepository extends JpaRepository<Cart, Integer> {

	Cart getByUserId(int userId);

}

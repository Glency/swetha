package com.hcl.shopforhome.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.shopforhome.model.OrderItems;

@Repository
public interface IOrderItemsRepository extends JpaRepository<OrderItems, Integer>{

}
